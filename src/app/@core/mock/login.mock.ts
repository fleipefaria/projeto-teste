import { ILogin } from '../models/login.interface';

export const LOGIN_DATA: ILogin[] = [
	{
		email: 'joao_pedro@gmail.com',
		password: 'joao123'
	},
	{
		email: 'carlinho-the-buyer@hotmail.com',
		password: 'C4rl!nh0'
	},
	{
		email: 'JoanaFlores@yahoo.com',
		password: 'um23456sete'
	}
];
