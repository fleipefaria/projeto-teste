import { Injectable } from '@angular/core';
import { IPurchaseableProduct } from 'src/app/@theme/components/add-product/add-product.component';

@Injectable({
	providedIn: 'root'
})
export class ShoppingCartService {
	public products: IPurchaseableProduct[] = [];

	constructor() { }

	/**
	 * Adiciona um produto ao carrinho de compras
	 * @param product Produto a ser adicionado ao carrinho de compras
	 */
	public addProduct(product: IPurchaseableProduct): void {
		if (!product.amount) {
			product.amount = 0;
		}
		this.products.push(product);
	}

	/**
	 * Remove um produto do carrinho de compras
	 * @param product Produto a ser removido do carrinho de compras
	 */
	public removeProduct(product: IPurchaseableProduct): void {
		this.products.splice(this.products.indexOf(product), 1);
	}

	/**
	 * Recupera o número total de itens do carrinho
	 */
	public getNumberOfProducts(): number {
		return this.products.reduce((previous, current) => previous += current.amount, 0);
	}
}
