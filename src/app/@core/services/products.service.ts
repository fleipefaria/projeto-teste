import { Injectable } from '@angular/core';
import { IProduct } from '../models/product.interface';
import { PRODUCT_DATA } from '../mock/product.mock';

@Injectable({
	providedIn: 'root'
})
export class ProductsService {
	public products: IProduct[] = [];

	constructor() { }

	/**
	 * Recupera a lista de produtos
	 */
	public getProducts(): Promise<void> {
		// trabalhando com promise para simular funcionamento assíncrono
		return new Promise((resolve) => {
			setTimeout(() => {
				this.products = PRODUCT_DATA;
				resolve();
			}, 300);
		});
	}

	/**
	 * Recupera o produto com o Id informado
	 * @param id Id do produto a ser recuperado
	 */
	public getProductById(id: number): Promise<IProduct> {
		// trabalhando com promise para simular funcionamento assíncrono
		return new Promise((resolve) => resolve(PRODUCT_DATA.find(item => item.id === id)));
	}
}
