import { Injectable } from '@angular/core';
import { ILogin } from '../models/login.interface';
import { LOGIN_DATA } from '../mock/login.mock';
import { BehaviorSubject } from 'rxjs';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { IPurchaseableProduct } from 'src/app/@theme/components/add-product/add-product.component';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	public isLoggedIn: boolean;
	public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);

	constructor(private modalService: NgxSmartModalService) { }

	/**
	 * Efetua uma tentativa de login com os dados informados
	 * @param data Dados de login
	 */
	public login(data: ILogin): Promise<void> {
		// retornando promise para simular funcionamento de API
		return new Promise((resolve, reject) => {
			const found = LOGIN_DATA.some(item => item.email.toLowerCase() === data.email.toLowerCase() && item.password === data.password);

			if (found) {
				this.isLoggedIn = true;
				this.loggedIn.next(true);
				resolve();
			} else {
				this.loggedIn.next(false);
				reject({ error: true, errorMessage: 'Usuário não-existente ou senha incorreta' });
			}
		});
	}
}
