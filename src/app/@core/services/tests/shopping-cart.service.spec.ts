import { TestBed } from '@angular/core/testing';

import { ShoppingCartService } from '../shopping-cart.service';
import { PRODUCT_DATA } from '../../mock/product.mock';

describe('ShoppingCartService', () => {
	let service: ShoppingCartService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ShoppingCartService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should add product', () => {
		service.addProduct(PRODUCT_DATA[2]);
		expect(service.products).toContain(PRODUCT_DATA[2]);
	});

	it('should remove product', () => {
		service.addProduct(PRODUCT_DATA[3]);
		service.removeProduct(PRODUCT_DATA[3]);
		expect(service.products).not.toContain(PRODUCT_DATA[3]);
	});

	it('should inform total number of products', () => {
		service.addProduct(PRODUCT_DATA[3]);
		service.addProduct(PRODUCT_DATA[2]);

		const randomNumber1 = Math.floor(Math.random() * 10);
		const randomNumber2 = Math.floor(Math.random() * 10);
		service.products[0].amount += randomNumber1;
		service.products[1].amount += randomNumber2;

		expect(service.getNumberOfProducts()).toBe(randomNumber1 + randomNumber2);
	});
});
