import { TestBed } from '@angular/core/testing';

import { ProductsService } from '../products.service';
import { PRODUCT_DATA } from '../../mock/product.mock';

describe('ProductsService', () => {
	let service: ProductsService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ProductsService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should list products', async () => {
		await service.getProducts();
		expect(service.products).toBeTruthy();
		expect(service.products.length).toBeGreaterThan(0);
	});

	it('should get product by id', async () => {
		const expectedProduct = PRODUCT_DATA[3];
		const product = await service.getProductById(expectedProduct.id);
		expect(product).toEqual(expectedProduct);
	});
});
