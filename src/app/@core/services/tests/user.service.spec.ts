import { TestBed, async } from '@angular/core/testing';

import { UserService } from '../user.service';
import { LOGIN_DATA } from '../../mock/login.mock';
import { first } from 'rxjs/operators';
import { NgxSmartModalModule } from 'ngx-smart-modal';

describe('UserService', () => {
	let service: UserService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [NgxSmartModalModule.forRoot()]
		});
		service = TestBed.inject(UserService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should login', async (done) => {
		const data = LOGIN_DATA[0];
		expect(await service.login(data));
		service.loggedIn
			.pipe(first())
			.subscribe((success) => {
				expect(success).toEqual(true);
				done();
			});
	});
});
