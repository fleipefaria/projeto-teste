import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';


const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'products',
	},
	{
		path: 'products',
		loadChildren: () => import('./products/products.module').then(m => m.ProductsModule),
	},
	{
		path: 'shopping-cart',
		loadChildren: () => import('./shopping-cart/shopping-cart.module').then(m => m.ShoppingCartModule),
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule { }
