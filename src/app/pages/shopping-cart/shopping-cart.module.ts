import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingCartRoutingModule } from './shopping-cart-routing.module';
import { ShoppingCartComponent } from './shopping-cart.component';
import { AddProductModule } from 'src/app/@theme/components/add-product/add-product.module';
import { DiscountLabelModule } from 'src/app/@theme/components/discount-label/discount-label.module';


@NgModule({
	declarations: [ShoppingCartComponent],
	imports: [
		CommonModule,
		AddProductModule,
		DiscountLabelModule,
		ShoppingCartRoutingModule
	]
})
export class ShoppingCartModule { }
