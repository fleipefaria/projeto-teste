import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/@core/services/products.service';
import { IProduct } from 'src/app/@core/models/product.interface';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
	public products: IProduct[] = [];

	constructor(private productsService: ProductsService) { }

	ngOnInit(): void {
		this.getList();
	}

	/**
	 * Recupera a lista de produtos do service
	 */
	private getList(): void {
		this.productsService.getProducts().then(() => {
			this.products = this.productsService.products;
		});
	}

}
