import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ListComponent } from './list/list.component';
import { ProductCardModule } from 'src/app/@theme/components/product-card/product-card.module';
import { AddProductModule } from 'src/app/@theme/components/add-product/add-product.module';
import { DetailComponent } from './detail/detail.component';
import { DiscountLabelModule } from 'src/app/@theme/components/discount-label/discount-label.module';


@NgModule({
	declarations: [ListComponent, DetailComponent],
	imports: [
		AddProductModule,
		ProductCardModule,
		CommonModule,
		ProductsRoutingModule,
		DiscountLabelModule,
	]
})
export class ProductsModule { }
