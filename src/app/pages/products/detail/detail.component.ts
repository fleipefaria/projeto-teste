import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/@core/services/products.service';
import { first } from 'rxjs/operators';
import { IPurchaseableProduct } from 'src/app/@theme/components/add-product/add-product.component';

@Component({
	selector: 'app-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

	public product: IPurchaseableProduct;

	constructor(private route: ActivatedRoute, private productsService: ProductsService) { }

	ngOnInit(): void {
		this.getProductInfo();
	}

	/**
	 * Recupera informações do produto pelo Id informado na rota
	 */
	private getProductInfo(): void {
		this.route.params
			.pipe(first())
			.subscribe(async params => {
				this.product = await this.productsService.getProductById(+params.id);
			});
	}

}
