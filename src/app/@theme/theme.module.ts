import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { CardComponent } from './components/card/card.component';

const COMPONENTS = [
	ButtonComponent,
	CardComponent,
];

@NgModule({
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS],
	imports: [
		CommonModule
	]
})
export class ThemeModule { }
