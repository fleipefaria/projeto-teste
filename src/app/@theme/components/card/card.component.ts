import { Component, OnInit, ContentChild, AfterContentInit, TemplateRef } from '@angular/core';

@Component({
	selector: 'app-card',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
	@ContentChild('header') header: TemplateRef<any>;
	@ContentChild('content') content: TemplateRef<any>;
	@ContentChild('footer') footer: TemplateRef<any>;

	constructor() { }

	ngOnInit(): void {
	}
}
