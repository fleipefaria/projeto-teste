import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { By } from '@angular/platform-browser';

const headerText = 'header test';
const contentText = 'content test';
const footerText = 'footer test';
@Component({
	template: `
		<app-card>
			<ng-template #header><span id="header">${headerText}</span></ng-template>
			<ng-template #content><span id="content">${contentText}</span></ng-template>
			<ng-template #footer><span id="footer">${footerText}</span></ng-template>
		</app-card>
	`
})
class CardContentTestComponent {
	@ViewChild(CardComponent, { static: true }) appCard: CardComponent;
}

describe('CardComponent', () => {
	let component: CardContentTestComponent;
	let fixture: ComponentFixture<CardContentTestComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				CardComponent,
				CardContentTestComponent,
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CardContentTestComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should render header', () => {
		const innerText = (fixture.nativeElement.querySelector('span#header') as HTMLSpanElement).innerText;
		expect(innerText).toContain(headerText);
	});

	it('should render content', () => {
		const innerText = (fixture.nativeElement.querySelector('span#content') as HTMLSpanElement).innerText;
		expect(innerText).toContain(contentText);
	});

	it('should render footer', () => {
		const innerText = (fixture.nativeElement.querySelector('span#footer') as HTMLSpanElement).innerText;
		expect(innerText).toContain(footerText);
	});
});
