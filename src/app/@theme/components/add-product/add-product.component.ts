import { Component, OnInit, Input } from '@angular/core';
import { IProduct } from 'src/app/@core/models/product.interface';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { UserService } from 'src/app/@core/services/user.service';
import { ShoppingCartService } from 'src/app/@core/services/shopping-cart.service';
import { first } from 'rxjs/operators';

export interface IPurchaseableProduct extends IProduct {
	amount?: number;
}

@Component({
	selector: 'app-add-product',
	templateUrl: './add-product.component.html',
	styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

	@Input() product: IPurchaseableProduct;

	constructor(public userService: UserService, private modalService: NgxSmartModalService, private shoppingCart: ShoppingCartService) { }

	ngOnInit(): void {
	}

	/**
	 * Acrescenta 1 à quantidade do produto, adicionando-o do service de carrinho conforme necessidade
	 */
	public addItem(): void {
		if (this.userService.isLoggedIn) {
			if (!this.product.amount) {
				this.product.amount = 0;
				this.shoppingCart.addProduct(this.product);
			}

			this.product.amount++;
		} else {
			this.loginAndAddItem();
		}
	}

	/**
	 * Diminui 1 da quantidade do produto, removendo-o do service de carrinho conforme necessidade
	 */
	public decreaseItem(): void {
		this.product.amount--;

		if (this.product.amount === 0) {
			this.shoppingCart.removeProduct(this.product);
		}
	}

	/**
	 * Exibe o modal de login e, caso o login seja efetuado com sucesso, adiciona o item
	 */
	private loginAndAddItem(): void {
		const modal = this.modalService.getModal('loginModal');
		const onLogin = this.userService.loggedIn.subscribe(logged => {
			if (logged) {
				this.addItem();
			}
		});

		modal.onAnyCloseEventFinished
			.pipe(first())
			.subscribe(() => onLogin.unsubscribe());

		modal.open();
	}

}
