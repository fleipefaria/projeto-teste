import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../theme.module';
import { AddProductComponent } from './add-product.component';



@NgModule({
	declarations: [AddProductComponent],
	exports: [AddProductComponent],
	imports: [
		CommonModule,
		ThemeModule
	]
})
export class AddProductModule { }
