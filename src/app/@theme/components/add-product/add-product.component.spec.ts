import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProductComponent } from './add-product.component';
import { PRODUCT_DATA } from 'src/app/@core/mock/product.mock';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { Component, ViewChild } from '@angular/core';
import { AddProductModule } from './add-product.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ModalsModule } from '../../modals/modals.module';

@Component({
	template: `
		<app-modal-wrapper></app-modal-wrapper>
		<app-add-product></app-add-product>
	`
})
class AddProductTestComponent {
	@ViewChild(AddProductComponent) addProduct: AddProductComponent;
}

describe('AddProductComponent', () => {
	let component: AddProductTestComponent;
	let fixture: ComponentFixture<AddProductTestComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				AddProductModule,
				ModalsModule,
				RouterTestingModule.withRoutes([])
			],
			declarations: [AddProductTestComponent, AddProductComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddProductTestComponent);
		component = fixture.componentInstance as any;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should add item', () => {
		// setando flag de login como 'true'
		component.addProduct.userService.isLoggedIn = true;

		const product: any = {...PRODUCT_DATA[0]};
		component.addProduct.product = product;
		component.addProduct.addItem();
		expect(product.amount).toEqual(1);
	});

	it('should decrease item', () => {
		const product = {...PRODUCT_DATA[0], amount: 2};
		component.addProduct.product = product;
		component.addProduct.decreaseItem();
		expect(product.amount).toEqual(1);
	});
});
