import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountLabelComponent } from './discount-label.component';
import { By } from '@angular/platform-browser';

describe('DiscountLabelComponent', () => {
	let component: DiscountLabelComponent;
	let fixture: ComponentFixture<DiscountLabelComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DiscountLabelComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DiscountLabelComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should inform when there is not a discount', () => {
		const discountlessDiv = fixture.nativeElement.querySelector('div.info-label--discountless');
		expect(discountlessDiv).toBeTruthy();
	});

	it('should inform when there is a discount', () => {
		component.amount = 10;
		fixture.detectChanges();
		const discountDiv = fixture.nativeElement.querySelector('div.info-label--discount');
		expect(discountDiv).toBeTruthy();

		const discountlessDiv = fixture.nativeElement.querySelector('div.info-label--discountless');
		expect(discountlessDiv).toBe(null);
	});
});
