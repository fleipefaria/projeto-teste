import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiscountLabelComponent } from './discount-label.component';



@NgModule({
	declarations: [DiscountLabelComponent],
	exports: [DiscountLabelComponent],
	imports: [
		CommonModule
	]
})
export class DiscountLabelModule { }
