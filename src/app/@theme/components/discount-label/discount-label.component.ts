import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-discount-label',
	templateUrl: './discount-label.component.html',
	styleUrls: ['./discount-label.component.scss']
})
export class DiscountLabelComponent implements OnInit {

	@Input() amount: number;

	constructor() { }

	ngOnInit(): void {
	}

}
