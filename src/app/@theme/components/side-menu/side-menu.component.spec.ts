import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideMenuComponent } from './side-menu.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { Component } from '@angular/core';
import { ModalsModule } from '../../modals/modals.module';
import { SideMenuModule } from './side-menu.module';
import { RouterTestingModule } from '@angular/router/testing';

@Component({
	template: `
		<app-modal-wrapper></app-modal-wrapper>
		<app-side-menu></app-side-menu>
	`
})
class SideMenuTestComponent {}

describe('SideMenuComponent', () => {
	let component: SideMenuTestComponent;
	let fixture: ComponentFixture<SideMenuTestComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				ModalsModule,
				RouterTestingModule.withRoutes([]),
				SideMenuModule,
			],
			declarations: [SideMenuTestComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SideMenuTestComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
