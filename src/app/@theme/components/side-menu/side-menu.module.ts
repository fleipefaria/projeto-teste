import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideMenuComponent } from './side-menu.component';
import { ThemeModule } from 'src/app/@theme/theme.module';
import { RouterModule } from '@angular/router';



@NgModule({
	declarations: [SideMenuComponent],
	exports: [SideMenuComponent],
	imports: [
		CommonModule,
		ThemeModule,
		RouterModule,
	]
})
export class SideMenuModule { }
