import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/@core/services/user.service';
import { NgxSmartModalService, NgxSmartModalComponent } from 'ngx-smart-modal';
import { Subscription } from 'rxjs';
import { ShoppingCartService } from 'src/app/@core/services/shopping-cart.service';

@Component({
	selector: 'app-side-menu',
	templateUrl: './side-menu.component.html',
	styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit, AfterViewInit, OnDestroy {
	public  visible = false;
	private modal: NgxSmartModalComponent;
	private subscription$: Subscription;

	constructor(public user: UserService, public shoppingCart: ShoppingCartService, private modalService: NgxSmartModalService) { }

	ngOnInit(): void {
	}

	ngAfterViewInit(): void {
		this.modal = this.modalService.getModal('loginModal');
		this.subscribeToLogin();
	}

	ngOnDestroy(): void {
		if (this.subscription$) {
			this.subscription$.unsubscribe();
		}
	}

	/**
	 * Exibe o modal de login
	 */
	public openModal(): void {
		this.modal.open();
	}

	/**
	 * Exibe ou esconde o side-menu, quando no modo responsivo
	 */
	public toggleSideMenu(): void {
		document.documentElement.style.setProperty('--mobile-menu-left-alignment', this.visible ? '-6.625rem' : '0');
		this.visible = !this.visible;
	}

	/**
	 * Se inscreve na observable de login do UserService
	 */
	private subscribeToLogin(): void {
		this.subscription$ = this.user.loggedIn.subscribe(logged => {
			if (logged) {
				this.modal.close();
			}
		});
	}

}
