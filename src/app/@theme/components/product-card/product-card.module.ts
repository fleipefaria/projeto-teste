import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCardComponent } from './product-card.component';
import { ThemeModule } from '../../theme.module';
import { RouterModule } from '@angular/router';
import { AddProductModule } from '../add-product/add-product.module';

@NgModule({
	declarations: [ProductCardComponent],
	exports: [ProductCardComponent],
	imports: [
		ThemeModule,
		RouterModule,
		CommonModule,
		AddProductModule,
	]
})
export class ProductCardModule { }
