import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCardComponent } from './product-card.component';
import { PRODUCT_DATA } from 'src/app/@core/mock/product.mock';
import { ProductCardModule } from './product-card.module';
import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';
import { NgxSmartModalService, NgxSmartModalModule } from 'ngx-smart-modal';
import { RouterModule } from '@angular/router';
import { ModalsModule } from '../../modals/modals.module';
import { Component, ViewChild } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

@Component({
	template: `
		<app-modal-wrapper></app-modal-wrapper>
		<app-product-card [product]="product"></app-product-card>
	`
})
class ProductCardTestComponent {
	public product = PRODUCT_DATA[0];
}

describe('ProductCardComponent', () => {
	let component: ProductCardTestComponent;
	let fixture: ComponentFixture<ProductCardTestComponent>;

	beforeEach(async(() => {
		registerLocaleData(ptBr, 'pt-BR');
		TestBed.configureTestingModule({
			declarations: [ProductCardTestComponent],
			imports: [
				ProductCardModule,
				RouterTestingModule.withRoutes([]),
				ModalsModule,
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductCardTestComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should render product info', () => {
		expect(fixture.nativeElement.querySelector('.product__header').textContent).toContain(PRODUCT_DATA[0].name);
	});
});
