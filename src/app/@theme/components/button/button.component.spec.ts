import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ButtonComponent } from './button.component';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';

const buttonText = 'Texto para teste';

@Component({
	template: `
		<app-button>
			<span>${buttonText}</span>
		</app-button>
	`
})
class ButtonContentTestComponent {}

describe('ButtonComponent', () => {
	let component: ButtonContentTestComponent;
	let fixture: ComponentFixture<ButtonContentTestComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ButtonComponent, ButtonContentTestComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ButtonContentTestComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should correctly render projected content', () => {
		const innerText = (fixture.nativeElement.querySelector('span') as HTMLSpanElement).innerText;
		expect(innerText).toContain(buttonText);
	});
});
