import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { ThemeModule } from '../theme.module';
import { ModalWrapperComponent } from './modal-wrapper.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';

const COMPONENTS = [
	LoginModalComponent,
	ModalWrapperComponent,
];

@NgModule({
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS],
	imports: [
		CommonModule,
		ThemeModule,
		FormsModule,
		NgxSmartModalModule.forChild(),
	]
})
export class ModalsModule { }
