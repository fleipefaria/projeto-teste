import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { NgxSmartModalService, NgxSmartModalComponent } from 'ngx-smart-modal';
import { UserService } from 'src/app/@core/services/user.service';

@Component({
	selector: 'app-login-modal',
	templateUrl: './login-modal.component.html',
	styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit, AfterViewInit {
	public email: string;
	public password: string;
	public errorMessage: string;
	private modal: NgxSmartModalComponent;

	constructor(private modalService: NgxSmartModalService, private userService: UserService) { }
	@ViewChild('wrapper', { static: true }) wrapper;

	ngOnInit(): void {
	}

	ngAfterViewInit(): void {
		this.modal = this.modalService.getModal('loginModal');
	}

	/**
	 * Fecha o modal de login
	 */
	public close(): void {
		this.modal.close();
	}

	/**
	 * Efetua tentativa de login
	 */
	public login(): void {
		this.userService.login({ email: this.email, password: this.password }).then(() => {
			this.errorMessage = '';
		}).catch((error) => this.errorMessage = error.errorMessage);
	}

}
