import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import ptBr from '@angular/common/locales/pt';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserService } from './@core/services/user.service';

import { SideMenuModule } from './@theme/components/side-menu/side-menu.module';
import { ModalsModule } from './@theme/modals/modals.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ShoppingCartService } from './@core/services/shopping-cart.service';
import { registerLocaleData } from '@angular/common';

const SERVICES = [
	UserService,
	ShoppingCartService,
];

const MODULES = [
	SideMenuModule,
	ModalsModule,
];

registerLocaleData(ptBr, 'pt-BR');

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		...MODULES,
		BrowserModule,
		AppRoutingModule,
		NgxSmartModalModule.forRoot(),
	],
	providers: [...SERVICES],
	bootstrap: [AppComponent]
})
export class AppModule { }
